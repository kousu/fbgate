#!/usr/bin/env python

from __future__ import print_function

import shlex

from signal import *

from sleekxmpp.xmlstream.matcher import StanzaPath, MatchXPath
from sleekxmpp.componentxmpp import *

from fb import *

# enable printing of stanzas as SleekXMPP processes them
import logging
logging.basicConfig(level=logging.DEBUG,
                                format='%(levelname)-8s %(message)s')


from sleekxmpp.plugins.xep_0172.stanza import UserNick
from sleekxmpp.plugins.xep_0085.stanza import *
# XXX monkey-patch over xep_0085: ChatState() and its descendents just make "< />" which is an invalid tag!
#     they seem to work fine when they are created by the magical XML stream processor callback gunk from a pre-written XMLL stream
#     but if you literally just want to make a single one of them in order to send XML, they are broken
#     it's also strange that UserNick *seems* to be basically identical, and it works as expected: str(UserNick()) == '<nick xmlns="http://jabber.org/protocol/nick" />'
del ChatState.setup

class AutoregisterEventHandlers:
    """
    A mixin to automatically registers methods named "on_something" to handle the "something" event.
    XXX this doesn't seem to work on py2? super() is wrong? somehow?
    """
    def __init__(self, *args, **kwargs):
        super(AutoregisterEventHandlers, self).__init__(*args, **kwargs)
        
        #logging.debug("Autoregistering handlers for %s" % (self,))
        for n, f in {n.replace("on_","",1): getattr(self, n) for n in dir(self) if n.startswith("on_")}.items():
            #logging.debug("%s.%s" % (self, f))
            self.add_event_handler(n, catch_and_release(f))
        #logging.debug("Done autoregistering handlers for %s" % (self,))

### TODO:
# thhink about better isolating different accounts we're handling from each other
# e.g. it should be impossible for their messages to cross


class Bridge(object):
    """
    Bridge a single Facebook account to a single JID

    Perhaps:
     - pass both the target JID, the xmpp side, the orca side
     - pass the transport
     - 
    Bridge(transport, "user@example.com", 0131310111, "sdpijfoidsjfoidsjfs")
    sets up event handlers for user@example.com to log in to facebook as facebook account 0131310111
    """
    def __init__(self, xmpp, jid, fbid, access_token):
        self._xmpp = xmpp
        self.jid = jid # this is a user JID, like "user@example.com"; it's *not* the transport
        self.fbid = fbid

        self._orca = Orcinus(fbid, access_token)

        # presence
        self._orca.on_presence = lambda p: self.on_incoming_presence(p)
        self.add_event_handler('presence', self.on_outgoing_presence)

        # typing notifications
        self._orca.on_typing = lambda p: self.on_incoming_chatstate(p)

        # messages
        self._orca.on_message = lambda message: self.on_incoming_message(message)
        self.add_event_handler('message', self.on_outgoing_message)

        self._orca.connect()
        self._orca.mqtt.loop_start() #XXX spawns a thread!!!

    def add_event_handler(self, event, handler):
        "a bridge-local version of ComponentXMPP.add_event_handler"
        "which ensures we only handle events for self.jid"
        def wrapped(stanza):
            if self.jid == stanza['from'].bare: # XXX do we need to handle stanza['to'] as well?
                return handler(stanza)
        return self._xmpp.add_event_handler(event, wrapped)

    def __del__(self):
        # TODO: unregister event handlers from the xmpp object
        # (we don't need to unregister the orca handlers because the orca object is a child and will die with us)
        pass

    def on_incoming_chatstate(self, chatstate):
        """
        Facebook->XMPP
        """
        stanza = self._xmpp.Message()
        stanza['from'] = "%s@%s" % (chatstate['sender_fbid'], self._xmpp.boundjid)
        stanza['to'] = self.jid
        stanza['type'] = "chat" # XXX is this true? necessary? what does Orca do on group chats?
        # Facebook only sends active/inactive, while ChatStates come in 5 flavours
        # - Active (essentially identical to <presence type='available' />)
        # - Composing (i.e. typing)
        # - Paused (i.e. stopped typing, but with text in the input field, maybe, depending on the client)
        # - Inactive (not typing and probably not looking at the window; similar to <presence type='available'><show>away</show></presence>)
        # - Gone (essentially <presence type='available'><show>xa</show></presence>)
        # Composing is generally rendered in clients by saying "user@domain.com is typing"
        # Paused by "user@domain.com has stopped typing"
        # Active, Inactive and Gone are generally rendered by not rendering anythingi
        # So obviously facebook active == composing
        # But what do we choose for inactive?
        #  Paused seems the most accurate during a conversation, but if the remote user walks away their chatstate will be hung at "stopped typing", which isn't..strictly..wrong..but seems janky.
        #   We could add a thread on a timer that sends Inactive after a while, but that's complicated
        #  Inactive seems semantically incorrect, but it makes a more stable UI come out
        #  Gone makes identical UI come out, but seems even more semantically incorrect
        # So, for expendiency:
        #   chatstate['state'] == 1 --> composing
        #   chatstate['state'] == 0 --> inactive
        stanza.append((Composing if chatstate['state'] else Inactive)())
        stanza.send()

    def on_incoming_presence(self, presence):
        """
        Facebook->XMPP
        Presence is a thrift object of type Presence
        """

        # Algorithm should be: when we receive a Roster result, if it's *complete*, then we wipe out the user's roster
        # and then rewrite it
        # (to be more efficient, diff the known roster)
        # remember: we know the user's roster because they send presences on connect

        from_jid = "%s@%s" % (presence.uid, self._xmpp.boundjid)
        # Hack: because [remote roster](http://xmpp.org/extensions/xep-0321.html) never got finished
        #  we can't control our remote contacts 
        # https://xmpp.org/extensions/xep-0100.html#rosters suggests type='subscribed' might work, but that it's deprecated and in testing not even pidgin listens to that so I think that's old information
        stanza = self._xmpp.Presence()
        stanza['from'] = from_jid
        stanza['to'] = self.jid
        """
        # send a subscribe, including a desired friendly name
        name = self._orca.profile(presence.uid)['name'] # <-- beware, expensive!
        nick = UserNick(); nick.set_nick(fb_profile['name']); stanza.append(nick)
        stanza['type'] = 'subscribe'
        stanza.send()
        """
        stanza['type'] = 'available' if presence.active else 'unavailable'
        stanza.send()

        """
        # send a http://xmpp.org/extensions/xep-0144.html#add
        # ^ this is the first gateway remote roster protocol
        # there are now 321 and 3...55?
        # http://xmpp.org/extensions/xep-0144.html#entities-gateway
        # TODO: we should also respond to presence probes with deletes..
        # - these can be batched
        # - pidgin *does not implement* xep 144
        # - sleekxmpp *does not implement* xep 144
        stanza = self._xmpp.Message()
        stanza['from'] = self._xmpp.boundjid
        stanza['to'] = self.jid
        #stanza['body'] = "suggesting you add %s <%s>" % (fb_profile['name'], from_jid) # DEBUG
        x = ET.Element('x', xmlns="http://jabber.org/protocol/rosterx")
        x.append(ET.Element('item', action="add", jid=from_jid, name=fb_profile['name']))
        stanza.append(x)
        #stanza.send()
        """


    def on_outgoing_presence(self, presence):
        """
        XMPP->Facebook
        this is when you inform the gateway itself that you're online
        """
        # ???
        # set yourself active? I guess? or inactive?

        print("on_outgoing_presence:", pformat(presence))
        # TODO


    def on_incoming_message(self, event):
        """
        Facebook->XMPP
        """
        # event is a json object with a bunch of stuff that I'm mostly ignoring

        # TODO: log timestamp(?)
        logging.debug("_on_orca_message(%s)" % (event,))
#{u'sender_fbid': 100006067083522, u'sender_name': u'Sunny Fgsfds', u'other_user_fbid': u'100006067083522', u'api_tags': [u'inbox', u'source:mobile'], u'short_source': 2, u'offline_threading_id': u'6223720059232875444', u'mid': u'm_mid.1483850493259:826f310333', u'coordinates': None, u'has_attachment': False, u'tid': u't_mid.1370824097357:ef0cbd9940dcfb2e53', u'group_thread_info': None, u'type': u'message', u'action_id': 1483850493259000000, u'body': u'Beep', u'timestamp': 1483850493259, u'thread_fbid': None, u'admin_snippet': u'', u'prev_last_visible_action_id': 0, u's': u'0', u'z': u'0', u'attachment_map': {}, u'share_map':

        stanza = self._xmpp.Message()
        stanza['from'] = "%s@%s" % (event['sender_fbid'], self._xmpp.boundjid.bare)
        stanza['to'] = self.jid
        stanza['type'] = 'chat'
        stanza['body'] = event['body']
        stanza['id'] = event['mid'] # message ID: (some) clients can use this to disambiguate their logs

        if event['sender_fbid'] == self._orca.uid:
            # it's just the server carbon'ing a message we sent from ourself to ourselves (i.e. all our connected devices)
            # soooo...
            #  carbon the message
            #return # XXX this code isn't working. Ref: https://xmpp.org/extensions/xep-0280.html#outbound shows wrapping the original message in a <forwarded> in a <sent> in a <message>; I think the forwarded+sent part is supposed to be covered by SentCarbon(), but it crashes on set_carbon_sent because, of course, it has no internal <forwarded> XML element. Is it just buggy??
            # actually not at all: it depends on xep_0297
            from sleekxmpp.plugins.xep_0280 import SentCarbon
            from sleekxmpp.plugins.xep_0297 import Forwarded
            # we need to special case the from/to fields in this case: it's *from* the user's real JID and *to*
            stanza['from'] = self.jid
            stanza['to'] = "%s@%s" % (event['other_user_fbid'], self._xmpp.boundjid.bare)
            wrapper = self._xmpp.Message().append(Forwarded().append(SentCarbon().append(stanza)))
            #wrapper['from'] = self.jid # this is illegal: external components can only forge messages from their assigned domain
            # but the example in the spec clearly shows the wrapper both from and to the same jid (well, one has a resource attached)
            wrapper['from'] = self._xmpp.boundjid # will this work??
            wrapper['to'] = self.jid
            wrapper.send()
            return
        # TODO: attach <thread> item to the message with event['tid'] in it
        stanza.send()

    def on_outgoing_message(self, stanza):
        """
        XMPP->Facebook
        """
        logging.debug("on_outgoing_message(%r)", stanza)
        if stanza['to'].domain == self._xmpp.boundjid:
            assert stanza['from'].bare == self.jid
            assert stanza['type'] == 'chat'
            self._orca.send(int(stanza['to'].user), stanza['body'])
        else:
            raise ValueError("Message sent through us that wasn't targetted to us: %r" % stanza)
            pass


class OrcaTransport(ComponentXMPP):
    """
    An XMPP external component to add Facebook Messenger support to the XMPP network
    """
    def __init__(self, jid, secret, host=None, port=None):
        super(OrcaTransport, self).__init__(jid, secret, host=host, port=port)

        self.auto_subscribe = True
        self.auto_authorize = True

        # FIXME
        # disable automagic presence responding because part of the
        # magic is to send <presence type='unsubscribed'> in reply to
        # unrecognized jids, which pisses off Gajim (and probably
        # others) who then think they should remove the contact from
        # their roster, or at least mark it degraded.
        #
        # The way Sleek wants us to handle this is to fill self.roster
        # before connecting. But that's impossible with a remote roster,
        # so I don't know, what the fuck?
        # For now, a stop gap: record everyone with presence to us as
        # having 'from' subscription (ie. we receive presence from it)
        def on_presence(stanza):
            if "subscribe" not in stanza['type']: # the four kinds of subscription stanzas: <presence type="(un)?subscribe(d)?">
                self.roster[stanza['to']][stanza['from']]['from'] = True
        self.add_event_handler('presence', on_presence) 
        # Here's another stopgap which is simpler but more invasive:
        #self.del_event_handler('presence_probe', self._handle_probe)

        self.add_event_handler("session_start", self.on_session_start)
        self.add_event_handler("got_online", self.on_got_online)
        self.add_event_handler("got_offline", self.on_got_offline)
        self.add_event_handler("message", self.on_message)

        self.register_plugin('xep_0085') # typing notifications
        self.add_event_handler('chatstate', self.on_chatstate)

        self.register_handler(
                Callback('Gateway Protocol Handler',
                         #MatchXPath('{jabber:component:accept}iq[@type=get]/{jabber:iq:gateway}query'), #.. this..makes the gateway disconnect
                         #MatchXPath('{jabber:component:accept}iq@type=get/{jabber:iq:gateway}query'), # this causes "KeyError: u'@'" which is obviously wrong
                         # StanzaPath('iq@type=get/gateway') # this requires understanding sleek's plugin architecture better than I do. ergh. i wish it wasn't *quite* as helpful...
                         MatchXPath('{jabber:component:accept}iq/{jabber:iq:gateway}query'),
                         # another gotcha here: the implicit {jabber:component:accept} which comes with the outer stream *because we're a component*
                          lambda iq: self.event('gateway_request', iq)))


        def gateway_request(iq):
            if iq['type'] == 'get':
                logging.debug('gateway_request on %r', iq)
                # https://xmpp.org/extensions/xep-0100.html#addressing-iqgateway
                iq = iq.reply()
                logging.debug('our reply stanza is %r', iq)
                prompt = ET.Element('prompt')
                prompt.text = "fuck you elementtree"
                desc = ET.Element('desc')
                desc.text = "if you want to stab your face into the ground, just use XML"
                query = ET.Element('query', xmlns="jabber:iq:gateway")
                query.append(prompt)
                query.append(desc)
                iq.append(query)
                iq.send()
        self.add_event_handler('gateway_request', gateway_request)
        self.register_plugin('xep_0030') # disco
        self['xep_0030'].add_identity(category='gateway', itype='facebook', jid=self.boundjid, name="~~~ Facebook Transport Sk$$llz ~~~", lang="en")

        # XXX TODO: implement sleekxmpp.plugins.xep_0144 as a proper module
        #self['xep_0030'].add_feature('http://jabber.org/protocol/rosterx')
        # XXX TODO: implement jabber:iq:roster as a proper sleek plugin..
        self['xep_0030'].add_feature('jabber:iq:roster')
        self['xep_0030'].add_feature('jabber:iq:gateway')

        self.register_plugin('xep_0077') # in-band registration
        # huh. this xep_0077 ..seems to only be client side??
        # there's a half-finished server-side copy used as a tutorial at http://sleekxmpp.com/create_plugin.html
        # but neither are very good; in particular, neither trigger events, but instead store data internally,
        # which makes them hard to adapt.
        # ergh.
        # oh wait! newer versions of sleek/slix have it https://github.com/fritzy/SleekXMPP/blob/develop/sleekxmpp/plugins/xep_0077/register.py
        # advertising this makes Gajim enable the 'register' button when you do service discovery
        self['xep_0030'].add_feature(self['xep_0077'].stanza.Register.namespace) 
        # of course, since we haven't done the rest, it breaks if you try to use it

        self._accounts = {} # facebook credentials
        self._sessions = {} # active facebook clients

    def on_chatstate(self, stanza):
        if stanza['to'].user: #<-- only pass on chatstates if they're to a user, not to the transport itself
            xmpp_sender = stanza['from'].bare
            fb_target = int(stanza['to'].user)
            name = self._sessions[xmpp_sender]._orca.profile(fb_target)['name']
            if stanza['chat_state'] == 'composing':
                # TODO: send 'active'
                logging.debug("%s is writing to %s" % (xmpp_sender, name))
            elif stanza['chat_state'] == 'paused':
                logging.debug("%s stopped writing to %s" % (xmpp_sender, name))
                # TODO: send 'inactive'
            # there are 3 other [chatstates](http://xmpp.org/extensions/xep-0085.html#definitions) but they are made redundant by <presence>, AFAICT.


    def on_session_start(self, event):
        print("gateway booted")

    def on_got_online(self, event):
        print("SOMEONE GOT ONLINE")
        if event['to'].bare != self.boundjid.bare: return

        print(event)
        # this means one of our users (maybe) connected
        # if so, start a facebook session for them, if they don't already have one
        # (but in theory, on_got_online only fires *when* they don't already have one)
        # send a presence
        p = self.Presence()
        p['to'] = event['from'].bare
        #p['from'] = self.boundjid
        p.send()

        # log in..
        # XXX how do we report login failures to the user? is that to do with presence somehow?
        user = event['from'].bare
        self._sessions[user] = Bridge(self, user, self._accounts[user]['uid'], self._accounts[user]['access_token'])

        # XXX this is supposed to ask the server for
        # but the prosody module *doesn't implement this*: it assumes that every contact on the same domain as the transport is the transport's responsibility
        # (and you know what? maybe that's better. you have to trust the server, anyway.)
        #manage_request = ET.Element("{urn:xmpp:tmp:roster-management:0}query")
        #manage_request = self.make_iq_set(ito=event['from'], sub=manage_request)
        #print(manage_request)
        #global G
        #G = manage_request
        #manage_request.send()

        get_roster = self.Iq()
        get_roster['to'] = event['from'].bare
        get_roster['from'] = self.boundjid
        get_roster['type'] = 'get'
        get_roster.enable('roster')
        print(get_roster)
        roster = get_roster.send()

        # here's the current roster..
        current_roster = roster['roster']['items']
        pprint(current_roster)

        for jid in current_roster:
            assert jid.domain == self.boundjid
            if jid == self.boundjid: continue
            erase_roster = self.Iq()
            erase_roster['to'] = event['from'].bare
            erase_roster['from'] = self.boundjid
            erase_roster['type'] = 'set'
            erase_roster.enable('roster')
            v = dict(current_roster[jid]) # copy-constructor
            v['subscription'] = 'remove'
            erase_roster['roster'].set_items({jid: v})
            erase_roster.send()

        # now, go get our roster off facebook
        facebook_roster = self._sessions[event['from'].bare]._orca.api.roster()
        #pprint(facebook_roster)

        #facebook_roster = facebook_roster[:10] # DEBUG: limit to 10 people

        # pre-process the facebook roster to drop excess data
        # TODO: there's a lot of data here, is extracting any more of it worthwhile
        # TODO: use friendship_status to decide on a more accurate subscription status
        #   (ARE_FRIENDS => 'both', CAN_REQUEST => 'to' ??)
        facebook_roster = {int(e['represented_profile']['id']):  # <-- this is a dictionary comprehension
                           {'name': e['structured_name']['text'], 'friendship_status': e['represented_profile']['friendship_status'] }
                           for e in facebook_roster}
        pprint(facebook_roster)
        #import time; time.sleep(5)

        # mock roster, to test XEP 321
        #facebook_roster = {1: {'name': 'Me', 'friendship_status': 'ARE_FRIENDS'}, 
        #                   2: {'name': 'Myself', 'friendship_status': 'ARE_FRIENDS'}, 
        #                   3: {'name': 'I', 'friendship_status': 'ARE_FRIENDS'},}
        facebook_roster = {
          "%s@%s" % (uid, self.boundjid):  # ditto
           {'name': facebook_roster[uid]['name'], 'subscription': 'both', 'groups': ['Facebook']}
           for uid in facebook_roster}

        #now, compare the two; in particular, we're looking for:
        # - people
        # (we could just erase *everyone* in our roster and send them anew)
        # XXX I wonder if I could just operate on get_roster directly?
        #   problem: we'd have to swap to and from addresses, and more importantly we'd have to generated a new Iq ID
        # we're only allowed to send one of these at a time, per https://tools.ietf.org/html/rfc6121#section-2.1.5
        for jid in facebook_roster:
            set_roster = self.Iq()
            set_roster['type'] = 'set'
            set_roster['to'] = event['from'].bare
            set_roster['from'] = self.boundjid
            set_roster.enable('roster')
            set_roster['roster'].set_items({jid: facebook_roster[jid]})
            set_roster.send()

    def on_got_offline(self, event):
        print("SOMEONE GOT OFFLINE")
        # send a presence
        # stop the facebook session


    def on_message(self, event):
        if event['to'].bare == self.boundjid:
            # message to us:
            # a command line
            user = event['from'].bare
            argv = shlex.split(event['body'].strip())
            print("command == ", argv[0])
            if argv[0] == "register":
                _, facebook_username, facebook_password = argv
                # TODO: trade username and pass for uid and token immediately
                self._accounts[user] = facebook_username, facebook_password
            elif argv[0] == "connect":
                if user in self._sessions:
                    return #??? probably not?!
                self._sessions[user] = Bridge(self, user, self._accounts[user]['uid'], self._accounts[user]['access_token'])
            elif argv[0] == "disconnect":
                self._sessions[user].disconnect()
                del self._sessions[user]
        else:

            pass
            pass


if __name__ == '__main__':
    #WORKAROUND: slixmpp dies for some reason if told to connect to "localhost":
    #   the DNS resolver gets 127.0.0.1 back but it sets the port to 0, which makes the connection attempt spin in place
    #   and curiously, if given a real domain name, the resolve just flatly fails
    #   to get around that, we precompute the resolve for Slix
    jabber = ("localhost", 5347)
    import socket
    jabber = (socket.gethostbyname(jabber[0]), jabber[1])

    # XXX the transport jid and password should come off the command line!
    transport = OrcaTransport("transport.requiem", "secret", jabber[0], jabber[1])
    transport._accounts["saul@requiem"] = load_auth()
    transport.auto_reconnect = False
    transport._use_daemons = True
    transport.connect()

    # Main loop
    # BUG: see experiments/bsd_thread_syscall_hang.py; the upshot of which is that there's no way to
    # quit this next line if block=True is on because the read(), on BSD-python, can't be interrupted (EINTR) by a signal 
    transport.process(block=False)
    # workaround: set block=False and then fall through to this loop, which forces
    # a system call on the main thread again, which seems to make it listen better.
    # (interestingly: read(1) or readline() requires typing Ctrl-C plus enter before the KeyboardInterrupt exception is thrown
    #  but read(0) makes it happen as soon as you type ctrl-c)
    import sys
    while True: sys.stdin.read(0)

    # NB: this version works basically the same, except it has to wait for the sleep() to timeout before processing an interrupt 
    #from time import sleep
    #while True: sleep(.1)

    # NB: the undocumented _use_daemons is key above; without it, when we get to this point, the program will keep running
    # because it will have several non-daemon threads handy
