TODO
====

* [x] Basic two-way private messaging
* [x] Name the users, instead of just using 100000000@facebook.transport.tld
* [ ] Invisibility
  * XMPP supported
* [x] Typing notifications
* [ ] Port to asyncio (slixmpp) over threads (sleekxmpp)
* [ ] Write proper sleekxmpp plugins for generating/handling the missing message types
* [ ] Roster sync
  * [ ] Announce ourselves as a jabber:x:gateway
  * [ ] Use the roster suggestions to auto-sync roster items between
  * [ ] Handle the case when someone deletes you while you're online
    * apparently nothing happens: you simply get no notice of them
  * Orca *doesn't* send you a complete contact list at boot
* [ ] Investigate alternate ways to get a facebook OAuth token
  * in particular, can I do the OAuth flow for apps *instead* of using an app password?
  * flow could be: "register" (either by the XMPPey way? or via a text account?) -> return a facebook URL to click -> login to facebook and receive the result ..on a callback?
* [ ] Implement XMPPey account registration 
* [x] Download facebook roster at connects
  * cache that shit
  * API.roster -> API.contacts since that's what facebook calls it
    (the xmpp side can relabel this to roster)
  * figure out who are the lost people
  * figure out the difference between the graph api and graphql (graphql returns more contacts than facebook claims I have----because it includes message req
* test support for message requests (e.g. do I need to do something special to show them as messages? xmpp has nothing in it like it, so my only choices are: ignore them, or accept all of them)
* Support attachments
* Connect to facebook over tor ((and ensure that, in this case, all facebook attachment URLs *are also loaded over tor*))
  * we're already connecting to facebook from a server, which hides where you are from them, but why not hide? it'll make good use of facebookcorewwwi.onion address...
* [ ] Install [XEP 321](ottps://modules.prosody.im/mod_remote_roster.html)
* [ ] 
* [ ] MAM?? (read facebook message history)
* [ ] Complain to the sleekxmpp authors about their assumption that rosters are loaded from persisted storage
      becaues that's bullshit for almost any gateway. a gateway should
      just be a relay, it shouldn't need state beyond account registration
      and even that we can probably elide if we're clever.
* [ ] Security: is it possible to architect the transport across forks? like,
  when a client connects set up pipes for IPC, then fork, so that the sub
  we'd make a BaseXMPPProxy object which serializes calls and sends them over the pipe
  and which receives calls

* [ ] xmppd:
 * [ ] detect invalid certs and prompt to regenerate
 * [ ] move certs/ under prosody/
 * [ ] make prosody/ a hidden folder?
* [ ] Timestamps:
  * [ ] user last seen: http://xmpp.org/extensions/xep-0256.html
  * [ ] message timestamps: http://xmpp.org/extensions/xep-0203.html
  * (Orca hands us both these values, we just need to map them over)
* [ ] Read receipts:
  * xmpp: https://xmpp.org/extensions/xep-0184.html
  * orca: I don't seem to currently be subscribed to whatever endpoint outputs the read receipts. I'll have to go digging.
	* It's possible purple-facebook never implemented that, since it's not a huge feature, and bitlbee-facebook definitely didn't since IRC has no read receipts


Politicking

* [ ] bug: Gajim refuses roster pushes (argh)
  * [ ] document this, by showing the logs when pidgin and gajim interact thus.
* [ ] Gajim: ignores roster version number (which is convenient, but wrong)
* [ ] push back against XEP-0321's permission system:
    * a) a transport implicitly; see also the older system that said gateways should 
    * a remote transport shouldn't have 
* [ ] In SleekXMPP, roster/ is a sketchy pile of object oriented indirection
* [ ] In sleekxmpp, .is_component is used wayyyyy too often inline. Rather than that shit, there should be subclasses that override or extend pieces of behaviour.


caching: http://pythonhosted.org/Brownie/api/caching.html
 wait, but i want a time-based cache....

-----

get znc saving state across server reboots (savebuff + something else should do it)'


