

from sleekxmpp.plugins.xep_0172.stanza import *

# This works
assert str(UserNick()) == '<nick xmlns="http://jabber.org/protocol/nick" />'



from sleekxmpp.plugins.xep_0085.stanza import *

# All of these fail: instead, they output "< />"
assert str(Active()) == '<active xmlns="http://jabber.org/protocol/chatstates" />'
assert str(Composing()) == '<composing xmlns="http://jabber.org/protocol/chatstates" />'
assert str(Paused()) == '<paused xmlns="http://jabber.org/protocol/chatstates" />'
assert str(Inactive()) == '<inactive xmlns="http://jabber.org/protocol/chatstates" />'
assert str(Gone()) == '<gone xmlns="http://jabber.org/protocol/chatstates" />'

# but doing this makes them work
del ChatState.setup
assert str(Active()) == '<active xmlns="http://jabber.org/protocol/chatstates" />'
assert str(Composing()) == '<composing xmlns="http://jabber.org/protocol/chatstates" />'
assert str(Paused()) == '<paused xmlns="http://jabber.org/protocol/chatstates" />'
assert str(Inactive()) == '<inactive xmlns="http://jabber.org/protocol/chatstates" />'
assert str(Gone()) == '<gone xmlns="http://jabber.org/protocol/chatstates" />'
