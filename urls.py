
# The HTTP host for the Facebook API.
FB_API_AHOST = "https://api.facebook.com"

# The HTTP host for the Facebook BAPI (???)
FB_API_BHOST = "https://b-api.facebook.com"

# The HTTP host for the Facebook Graph API
FB_API_GHOST = "https://graph.facebook.com"

# The HTTP host for the Facebook website
FB_API_WHOST = "https://www.facebook.com"

# The Facebook API key (aka the app's unique ID)
FB_API_KEY = "256002347743983"

# The Facebook API secret
# Not really all that secret if it's here in the source code
FB_API_SECRET = "374e60f8b9bb6b8cbb30f78030438895"

# The HTTP User-Agent header.
FB_API_AGENT = "Facebook plugin / BitlBee"


# The URL for attachment URL requests
FB_API_URL_ATTACH = FB_API_AHOST + "/method/messaging.getAttachment"

# The URL for authentication requests.
FB_API_URL_AUTH = FB_API_BHOST + "/method/auth.login"

# The URL for thread topic requests.
FB_API_URL_TOPIC = FB_API_AHOST + "/method/messaging.setthreadname"


# The URL for GraphQL requests
FB_API_URL_GQL = FB_API_GHOST + "/graphql"

# The URL for linking message threads
FB_API_URL_MESSAGES = FB_API_WHOST + "/messages"

# The URL for participant management requests.
FB_API_URL_PARTS = FB_API_GHOST + "/participants"

# The URL for thread management requests.
FB_API_URL_THREADS = FB_API_GHOST + "/me/threads"

