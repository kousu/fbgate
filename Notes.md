
Original reverse engineering of facebook's new [MQTT]()-based protocol is in libpurple:
https://bitbucket.org/pidgin/main/src/85a47f338c55b29cad8f7f9f76afd3f70b7d4cc5/libpurple/protocols/facebook/?at=default
(there's a [backport](https://github.com/dequis/purple-facebook) to libpurple2, i.e. the currently deployed one, but it just consists of automake + patches)
The same code has been copy-pasted(!) [into bitlbee](https://github.com/bitlbee/bitlbee-facebook/tree/master/facebook), and tweaked to be a cunt.

There's a python MQTT implemenation: https://mosquitto.org/documentation/python/
Maybe the best thing to do is to port bitlbee-facebook into facebook_messenger.py and then plug that into MQTT.

# TODO: tell bitlbee-facebook that they don't need the huge data structure they pass in; MQTT accepts your oauth token as a password

# TODO: [x] thriftpy
# TODO: [x ] make an external API (wrapper!)
#  --> [x] current roster tracking
#  --> [x] registering for message and presence notifications
#    --- instead of subscribing to, it's stateless: it just forwards on messages. the GUIs can deal with state.
# TODO: [x] uid -> profiles (so that I can say "xxx became active; xxx became inactive" instead of a user ID;
#    usernames (use these instead of uids in the jids? e.g. nick.nevering@fb.bridge.im instead of 100003243344334@fb.bridge.im)
#    Facebook deprecated these: http://stackoverflow.com/questions/23428498/get-username-field-in-facebook-graph-api-2-0#24506458
#     butttt since I'm logged in, it might still be usable?
# TODO: attachments (i.e. photos, stickers)
#   -> incoming
#   -> outgoing?? (on the XMPP side there's like 7 different attachment protocols: jingle in its 4 major variants, that inline version, HTTP Upload, SPIM or whatever that was just proposed...)
# TODO: invisibility
# TODO: read receipts
# TODO: axoltl!!!
# Remote roster? https://xmpp.org/extensions/xep-0321.html (this spec is incomplete...)

I think there's a bug in thriftpy:
struct Presences {
  1: bool update,
  2: list<Presence> roster,
}
vs
struct Presences {
  2: list<Presence> roster,
  1: bool update,
}
behave differently between thrift and thriftpy: thrift seems to handle the reordering fine, thriftpy mis-parses serialization





Transport/gateway tips:
https://xmpp.org/extensions/xep-0100.html
https://github.com/legastero/Weld/blob/master/weld/transport.py
o


Handling remote rosters:

Option 1: send <presence type='subscribe'> or <presence type='unsubscribe'> from the remote users
 problems with this:
  1) it prompts the user instead of happening automagically
  2) unsubscribing doesn't delete the user from the roster, it just means they'll vanish from the active list
Option 2:
https://xmpp.org/extensions/xep-0100.html#rosters + http://xmpp.org/extensions/xep-0144.html#entities-gateway
 says: if a gateway (a JID that advertises itself via disco as "gateway") advertises via disco http://jabber.org/protocol/rosterx, then
 xep0144 is allowed to happen without user approval
 problems:
 1) maybe this only works if (the xep is unclear; the wording "a user MUST first register with the gateway. If the gateway advertises support for a service discovery feature of 'http://jabber.org/protocol/rosterx'" sort of suggests that this logic only kicks in
 2) how widespread is support for xep 144?

Option 3:
http://xmpp.org/extensions/xep-0321.html
 problems:
 1) never finished, not supported
