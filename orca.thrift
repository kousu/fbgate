# Data structure definitions
# To turn into usable code, install Apache Thrift and run this:
# $ thrift --gen py:new_style -out . orca.thrift
# 

struct Presence {
  1: i64 uid,    # Facebook user ID; https://facebook.com/profile.php?id=<THIS>
  2: i32 active, # presence status: 1: available
  3: optional i64 timestamp, # last seen, in seconds since the unix Epoch
  4: optional i16 client_flags, # I don't know
  5: optional i64 VoIP_flags,   # ditto, but seems to only come along for people on Messeneger, i.e. who have Facebook VoIP calls available
}


struct Presences {
  1: bool update,
  2: list<Presence> roster,
}
