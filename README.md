Facebook XMPP Gateway
======================

[ desc, goals, limitations here ]

Installation
------------

[ configuring prosody and ejabberd ]
[ running ]
[ offering a remote service ]

Usage
-----

[ Facebook per-app passwords ]

Development
-----------

To develop this server, you will need a local copy of [prosody](https://prosody.im).

`pip install sleekxmpp`
(TODO: Run `pip install -r requirements.txt` )

To work on the server,

```
$ test/xmppd
```
This will generate self-signed certs and put up a demo server locally,
hosting domain `$(hostname)`, and adds two test users anna and saul both
with password "secret".
(so e.g. if your laptop is BobsMacbookPro, you will have jids anna@BobsMacbookPro
and saul@BobsMacbookPro).
It will also open up the XMPP external component port 5347 listening for
the gateway to plug itself in, also with password 'secret'.

Open up your XMPP client (any and eventually all of Pidgin, Gajim, Psi,
Conversations, Xabber must work with this) and login as saul@$(hostname)

Then put up the transport.

```
$ python -i transport.py
```
will connect the transport. You can either use your client's XMPP service
discovery button, or just add "transport.$(hostname)" as a contact to begin
using the transport.
At boot it will ask you for your facebook username and password.
Consider going to security settings.
An OAuth token received in return for this will be cached to auth.yml;
this is not as strong as a full facebook password, but it allows almost
unfettered access to the entire facebook API as your account, so be careful.

Right now only one user is supported, and it's hardcoded to be saul.
saul will log in as your facebook account upon receiving <presence>.


