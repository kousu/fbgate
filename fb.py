#!/usr/bin/env python2

from __future__ import print_function
from __future__ import division

import locale
locale.setlocale(locale.LC_ALL,"") # default the locale to the system locale; needed at login time to get the language right

import sys

import hashlib
import random
import time, datetime

# protocols
import requests  # HTTP
import paho.mqtt.client # MQTT
  # TODO: python3 this part
from urlparse import *

# serializations
import json           # JSON
from thrifts import * # Apache Thrift (nb: this is a local import)

# debug
import traceback
from pprint import *

import logging

# local
from urls import *

# TODO: this shit goes into api.py, or maybe fb/api.py

class FBAPIException(Exception):
    def __init__(self, code, msg=""):
        self.code = code
        self.message = msg
    def __str__(self):
        return "FBAPIException(%d, %s)" % (self.code, self.message)




class FBAPI(object):
    def __init__(self, access_token=None):
        self._http = requests.Session()
        self._http.headers['User-Agent'] = FB_API_AGENT
        if access_token:
            self._http.headers["Authorization"] = "OAuth %s" % (access_token,) #XXX copypasted from login()


    @staticmethod
    def _signature(api_key, api_secret, params):
	"""
	Compute a signature over a dictionary
	The signature is made by concatenating the dictionary entries *almost* like a URL query-string, then adding the key to the end of that, and hashing the whole thing
	Strangely, the choice of hash is apparently md5?? (md5 is only collision-broken, which means someone would have to have the full source, including the secret key, to abuse it)
	"""
	params = dict(params)
	assert "api_key" not in params
	params["api_key"] = api_key
	# ported from https://github.com/bitlbee/bitlbee-facebook/blob/ece0715947de3e11c5a726131dcd91900e986f98/facebook/facebook-api.c#L705
	# (notice that there are *no &-delimiter* between entries; this counterintuitive by just fine:
	# this is just for *hashing*, so the particular form doesn't really matter so long as it includes
	# all the data you want to verify; it doesn't even really need the = signs
	flatt = "".join(["%s=%s" % (key, params[key]) for key in sorted(params.keys(), key=str.lower)])
	#print(flatt)
	params['sig'] = hashlib.md5(flatt + api_secret).hexdigest()
	#print(params['sig'])
	return params

    def _req(self, url, name=None, data=None, method=None):
        """
        helper to add common headers for all HTTP requests
        
        method: a *facebook API* method, like auth.login or messaging.setthreadname
          this function *always* talks to api.facebook.com
          ..wait, maybe that's a mistake.
          i have no idea.
          i'll cross that bridge when i get to it.
        """
        assert locale.getlocale()[0] is not None # badness will happen if we pass in None where we need a string

        if method is None:
            if urlparse(url).path.startswith("/method/"):
                method = urlparse(url).path.replace("/method/", "", 1) # eat off the first part of the method
            else:
                method = "get"
        
        if data is None:
            data = {}

        data.update({
          "device_id": "Conversations.im. Or is it Pidgin??", #????
          "format": "json",
          "method": method, # ??? the facebook graph api debugger sends "get" for this (but to graph.facebook.com, not api.facebook.com...but one is CNAME'd to the other, soooo); bitlbee-facebook /seems/ to send "auth.login"???
          "locale": locale.getlocale()[0] # TODO: pull this from the system
          })
        if name:
            data["fb_api_req_friendly_name"] = name # not required?? doesn't seem to do anything.
        # wtf? in bitlbee-facebook sometimes 'method' looks like an HTTP verb, and sometimes it looks. and sometimes fb_api_req_friendly_name seems to be cosmetic, and other times it seems to carry an important subcommand. what is going on?
        # I guess I'll figure it out as I go.
        
        # sign the request parameters with the app credentials
        data = self._signature(FB_API_KEY, FB_API_SECRET, data)
        
        #pprint.pprint(data) #DEBUG
        # TODO: 
        # once logged in, you get 'token' which is an OAuth authorization cookie
        # which should be saved and then be attached as an HTTP header whenever available:
        # if (priv->token != NULL) {
        #   hdrs = fb_http_request_get_headers(req);
        #   fb_http_values_set_strf(hdrs, "Authorization", "OAuth %s", priv->token);
        #} 

        logging.debug("Making request to Facebook API: %r with:\n%r", url, pformat(data))
        r = self._http.post(url,
                             data=data,
                             )
        r.raise_for_status()
        if 'error_code' in r.json():
                raise FBAPIException(r.json()['error_code'], r.json()['error_msg'])
        return r.json()

    def query(self, query, query_params):
        """
        """

        # list of magic numbers
        # from https://github.com/bitlbee/bitlbee-facebook/blob/master/facebook/facebook-api.h#L140 onward
        QUERY_CODES = {
            'UsersQuery':10153915107411729, # get a contact's profile
            'FetchContactsFullQuery':10154444360806729, # get roster
            'FetchContactsFullWithAfterQuery':10154444360816729, # continuation of roster (it's batched, so you need to call it with a cursor repeatedly)
            }
        if query not in QUERY_CODES:
            raise ValueError("Unknown %s query %s" % (FB_API_URL_GQL, query))

        #logging.debug("%s(%s)", query, pformat(query_params))
        r = self._req(FB_API_URL_GQL,
                name=query,
                data={"query_id": QUERY_CODES[query], "query_params": json.dumps(query_params) })
        #logging.debug("%s(%s) = %s", query, pformat(query_params), pformat(r))
        return r

    def login(self, user, password):
        """
        given facebook username and password (where the password, notably, is allowed to be an ephemeral *per-app password*: https://www.facebook.com/settings?tab=security&section=per_app_passwords
        return a facebook (integer) user id and ephemeral access token
        in other words, trade one username/password pair for another, but where the other pair is what the rest of Facebook's systems will accept, to prevent constantly sending the master password on the wire.

        returns login cookie credentials
        """
        # TODO: handle errors
        logging.info("Logging into Facebook's API as %s" % (user,))
        auth = self._req(FB_API_URL_AUTH, name="authenticate", data={"email": user, "password": password})
        #print(auth) # DEBUG
        #pprint(auth.json()) #DEBUG
        self._http.headers["Authorization"] = "OAuth %s" % (auth['access_token'])
        return int(auth['uid']), auth['access_token']

    def logout(self):
        raise NotImplementedError("Instead of logging out, just delete this object.")

    def contact(self, uid):
        logging.debug("FBAPI.contact(%r)" % (uid,))
        contacts = self.query("UsersQuery", [str(uid), True])
        assert set(contacts.keys()) == {str(uid)}
        return contacts[str(uid)]

    def roster(self):
        """
        Get the roster for the logged in user
        """
        logging.debug("FBAPI.roster")
        BATCH_SIZE = 500
        roster = []
        batch = self.query("FetchContactsFullQuery", ["user", BATCH_SIZE])
        while batch['viewer']['messenger_contacts']['page_info']['has_next_page']:
            #print("BATCH has %d nodes" % (len(batch['viewer']['messenger_contacts']['nodes']))) # DEBUG
            roster.extend(batch['viewer']['messenger_contacts']['nodes'])
            batch = self.query("FetchContactsFullWithAfterQuery", ["user", batch['viewer']['messenger_contacts']['page_info']['end_cursor'], BATCH_SIZE])
        #print("BATCH has %d nodes" % (len(batch['viewer']['messenger_contacts']['nodes']))) # DEBUG
        roster.extend(batch['viewer']['messenger_contacts']['nodes'])
        # roster is sorted by ['represented_profile']['communication_rank']
        return roster

    def roster2(self):
        """
        Get the roster of the logged in user

        This uses graph.facebook.com instead of api.facebook.com/graphql
        """
        logging.debug("FBAPI.roster2")
        roster = []
        r = self._req("https://graph.facebook.com/v2.8/me/friends", data={'debug': 'all'})
        while 'next' in r['paging']:
            roster.extend(r['data'])
            r = self._req(r['paging']['next'], data={"debug": "all"})
        roster.extend(r['data'])
        return roster


class OrcaThread(object):
	"""
	Represents
	This is important because MQTT sometimes duplicates packets.
	"""
	def __init__(self, orca, tid):
		self.parent = orca
		self.id = tid
		self._timestamp = 0 # timestamp of the most recent message we've seen
							# note: we're assuming that MQTT sends messages in order, if not necessarily one at a time

	def _process_message(self, event):
		assert event['tid'] == self.id
		if self._timestamp < event['timestamp']:
			self._timestamp = event['timestamp']
			# forward it on
			# ..this seems awkward, with the callback 
			self.parent.on_message(event)


class Orcinus(object):
    """
    A client for Facebook Messenger.
     (Facebook's codename for this is Orca: the Android ID is com.facebook.orca and a bunch of the MQTT channels start with orca_; Orcinus is the genus the Orca is in)
    """
    def __init__(self, uid, access_token):
        
        # our facebook user ID (this is necessary to disambiguate self-messages and such)
    	self.uid = uid

        self.api = FBAPI(access_token)
    	
        logging.debug("Making Orcinus object for '%s' with access token '%s'" % (uid, access_token,))
    	self.mqtt = paho.mqtt.client.Client()
    	self.mqtt.username_pw_set(str(self.uid), str(access_token)) #NOTE: we are purposely forgetting the token after this point
        # Weeeeeeird:
        #self.mqtt.username_pw_set(str(self.uid)+"a", access_token) # works
        #self.mqtt.username_pw_set(str(self.uid), access_token+"garbage") # works
        #self.mqtt.username_pw_set(str(self.uid), access_token[:-1]) # works
        #self.mqtt.username_pw_set(str(self.uid), access_token[:-2]) # works
        #self.mqtt.username_pw_set(str(self.uid), access_token[:-3]) # doesn't work (and neither any shorter)

        # we should *also* create a requests.Session here, and stash the access token into its saved Headers
        # or actually I think I should probably wrap the Facebook API (that is, api.facebook.com/graph.facebook.com) into its own class, which internally keeps a requests.Session
    	
    	# wire up event handlers
    	self.mqtt.on_connect = self._on_mqtt_connect
    	self.mqtt.on_message = self._on_mqtt_message

        # data
        self._roster = {} #TODO: cache for profile()s: everyone we see we snitch the profile of
        self._inbox = {} #TODO: fill
        self._threads = {}
        self._profile_cache = {} # TODO: use a cache object with a timeout so that stale data is *sometimes* regotten


    
    # Login flow: trade your username/password to b-api.facebook.com/method/auth.login for an auth cookie
      # -> note: you should be able to use a https://www.facebook.com/settings?tab=security&section=per_app_passwords here..
      # you can save the auth cookie for a long time, several months, unless it's invalidated
      # it gives back a json structure with .uid (an 32 bit (??) ithat's only 4 billion?? that's not enough!) and $.access_token
    # on each connection pass that cookie in along with the other auth data to MQTT
      # -> the advantage of this design is that your password is only sent rarely, so it's a lot harder to sniff out, and the auth cookies can be invalidated easily

    @classmethod
    def by_login(cls, email, password):
        logging.debug("by_login(%r, %r)" % (email, password)) # XXX dangerous
    	uid, token = FBAPI().login(email, password)
    	return cls.by_token(uid, token)
    
    @classmethod
    def by_token(cls, uid, access_token):
        logging.debug("by_token(%r, %r)" % (uid, access_token))
    	return cls(uid, access_token)
    
    def connect(self):
        logging.debug("Orcinus.connect()")
        self.mqtt.tls_set("/etc/ssl/cert.pem") #XXX why do I have to hardcode this? shouldn't my openssl lib just find it?
        self.mqtt.connect("mqtt.facebook.com", 443)
        # from https://github.com/bitlbee/bitlbee-facebook/blob/ece0715947de3e11c5a726131dcd91900e986f98/facebook/facebook-api.c#L1018
        # mqtt doesn't provide a discovery mechanism; you just have to know; or maybe provide a custom discovery topic
    	
    def _on_mqtt_connect(self, client, userdata, flags, rc):
        logging.debug("_on_mqtt_connect(%r,%r,%r,%r)" % (client, userdata, flags, rc))
        assert client is self.mqtt
        if rc != 0:
            logging.debug("connect failed; error code: %d. disconnecting." % (rc,))
            self.mqtt.disconnect()
        
        logging.debug("Subscribing to MQTT events")
        self.mqtt.publish("/foreground_state", json.dumps({"foreground": True, "keepalive_timeout": 60}))        
        self.mqtt.subscribe([
            ("/inbox", 0),
            ("/mercury", 0),
            ("/messaging_events", 0),
            ("/orca_presence", 0),
            ("/orca_typing_notifications", 0),
            # https://github.com/bitlbee/bitlbee-facebook/blob/ece0715947de3e11c5a726131dcd91900e986f98/facebook/facebook-api.c#L1033 warns 
            # "Notifications seem to lead to some sort of sending rate limit" so be on the lookout for that
            # though..it seems obvious that com.facebook.orca *is uses orca_message_notifications to get messages*
            ("/orca_message_notifications", 0),
            ("/pp", 0),
            ("/t_ms", 0),
            ("/t_p", 0), # this is apparently also presence? /orca_presence isn't used??
            ("/t_rtc", 0),
            ("/webrtc", 0),
            ("/webrtc_response", 0),
            ])

        # make a table of callbacks to handle our subscriptions
        # ..this seems like there's a middle layer of abstraction we need:
        # these should be attached as methods on self.mqtt, which should just find them automagically by looking at the topic 
        # it parses, and then passes the parsed result up to...I guess a further layer of callbacks?
        self._mqtt_handlers = {'/orca_typing_notifications': self._on_orca_typing_notifications,
                '/orca_message_notifications': self._on_orca_message_notifications,
                '/t_p': self._on_presence,
                '/inbox': self._on_inbox
                }
    
    def _on_mqtt_message(self, client, userdata, message):
        if message.topic in self._mqtt_handlers:
            self._mqtt_handlers[message.topic](message.payload)
        else:
            logging.debug("%s %r" % (message.topic, message.payload))

    ############################
    # parsers for specific MQTT channels
    # https://github.com/bitlbee/bitlbee-facebook/blob/ece0715947de3e11c5a726131dcd91900e986f98/facebook/facebook-api.c#L1662
    
    def _on_orca_typing_notifications(self, payload):
        payload = json.loads(payload)
        assert 'type' in payload
        assert 'sender_fbid' in payload
        assert 'state' in payload #XXX these asserts should get caught because they are network errors, not internal errors
        # TODO: download mapping of friends->thing
        assert payload['type'] == 'typ'
        assert payload['state'] in [0,1]
        assert isinstance(payload['sender_fbid'], int)
        self.on_typing(payload)
        

    def _on_orca_message_notifications(self, payload):
        payload = json.loads(payload)
        #pprint(payload)
        if payload['type'] == 'message':
            if payload['tid'] not in self._threads:
                self._threads[payload['tid']] = OrcaThread(self, payload['tid'])
            self._threads[payload['tid']]._process_message(payload)
        else:
            pass
            pprint(payload)
        sys.stdout.flush()

    def _on_presence(self, payload):
            # the presence payload is not json, it's a thrift object
            # format can be implied from https://github.com/bitlbee/bitlbee-facebook/blob/ece0715947de3e11c5a726131dcd91900e986f98/facebook/facebook-api.c#L1539
            
            # this is very odd: in the sample Presences packets I have,
            # every one starts with a nul byte:
            # \x00\x11\x19\x1c\x16\xd4\xde\x93\xfe\x03\x15\x00\x16\xbc\xe9\xb7\x86\x0b\x00\x00
            # the nul byte is skipped explicitly by https://github.com/bitlbee/bitlbee-facebook/blob/ece0715947de3e11c5a726131dcd91900e986f98/facebook/facebook-api.c#L1550
            # which has the cryptic comment /* Read identifier string (for Facebook employees) */
            # (a single nul byte is a valid thrift string: thrift uses fortran strings,
            #  so a single nul byte is the string of length 0 (=null))
            # if we start the parse from 0, it breaks.
            # I've tested this against thrift and thriftpy, and they parse wrong, and my custom parser in thrifts.py only worked because I specifically ate exactly one byte manually in the Presences type.
            # So, I dunno. Here is my workaround:
            payload = payload[1:]
            
            presences = parse_presences(payload)
            if not presences.update: # this must be a *complete roster* if it's not an update
                self._roster = {}    # so wipe the roster
            for r in presences.roster:
                self._roster[r.uid] = r
                self.on_presence(r)
       

    def _on_inbox(self, payload):
        payload = json.loads(payload)

        print("Inbox status:")
        pprint(payload)
        # TODO: record the unread count, etc
        self._inbox.update(payload)
        print(self._inbox)

    
    @staticmethod
    def msgid(): 
        "make a new unique message ID"
        # https://github.com/bitlbee/bitlbee-facebook/blob/ece0715947de3e11c5a726131dcd91900e986f98/facebook/facebook-api.c#L2119
        msgid = int(time.time()), random.randint(0, 2**31)
        msgid = msgid[0] << 22 | (msgid[1] & 0x3FFFFF) # pack the tuple into a single integer via bitshift hax
        return msgid

    # API
    def send(self, to, body, attachments=None):
        to = int(to)
        assert isinstance(to, int) and 0<to<10**16
        assert isinstance(body, str)

        self.mqtt.publish("/send_message2", json.dumps({"sender_fbid": self.uid, "to": to, "body": body, "msgid": self.msgid()}))

    # API: reception; override these
    def recv(self, frm, callback):
        "register to receive messages from uid frm"
        pass

    def on_message(self, message):
        pass
        logging.debug("Orcinus.on_message(%r)" % (message,))

    def on_typing(self, typing):
        pass
        logging.debug("Orcinus.on_typing(%r)" % (typing,))

    def on_presence(self, presence):
        pass
        logging.debug("Orcinus.on_presence(%r)" % (presence,))

	
    def profile(self, uid):
        """
        Scrape together an object containing as much of the user uid's profile
        uid should be a *numeric facebook account ID*
        """
        # TODO: use a cache that times out old data
        if uid not in self._profile_cache:
            self._profile_cache[uid] = self.api.contact(uid)
        return self._profile_cache[uid]

    def loop_forever(self):
        self.mqtt.loop_forever()



def load_auth():
    "BEWARE: there is UI here"
    import yaml
    from getpass import getpass

    # the subsidiary login cookie is cached (unencrypted!) to disk
    # but we never cache the master facebook password
    # this mimics how the real Orca, and all other Facebook apps, behave:
    # ask for user auth once, get a cookie that's good for a few months, forget the master password.
    try:
        auth = yaml.load(open("auth.yml"))
    except IOError:
        credentials = {'email': raw_input("Email: "), 'password': getpass("Password: ")}

        fb = FBAPI()
        auth = fb.login(credentials['email'], credentials['password'])
        auth = {'uid': auth[0], 'access_token': str(auth[1])}
        with open("auth.yml","w") as o:
           yaml.dump(auth, o)

    return auth

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                              format='%(levelname)-8s %(message)s')
    auth = load_auth()
    O = Orcinus.by_token(auth['uid'], auth['access_token'])
    def on_typing(typing):
        #pprint(typing) # TODO: can I make this on_typing(sender, typing) ? or is there more metadata here?
        print("%s is %styping" % (typing['sender_fbid'], ["not ",""][typing['state']]))
    O.on_typing = on_typing
    def on_message(message):
        # NB: timestamps are in milliseconds since epoch; python uses seconds since epoch but allows them to be floating point
        print("[%s] %s: %s" % (datetime.datetime.fromtimestamp(message['timestamp']/1000.).ctime(), message['sender_name'], message['body']))
    O.on_message = on_message
    def on_presence(presence):
        p = O.api.contact(presence.uid)
        pprint(p)
        print("%s <%s>: %s" % (p['name'], p['profile_pic_large']['uri'], presence))
    O.on_presence = on_presence

    O.connect()
    #O.send(100005729972741, "test message.")
    logging.debug("test contact profile getting: %s", O.api.contact(100005729972741))
    roster = O.api.roster()
    #from collections import namedtuple
    #E = namedtuple('R', ['id','name','communicationRank','withTaggingRank','friendship','subscribed'])
    #simple_roster = [E(int(e['represented_profile']['id']), e['structured_name']['text'], e['represented_profile']['communicationRank'], e['represented_profile']['withTaggingRank'], e['represented_profile']['friendship_status'], e['represented_profile']['subscribe_status']) for e in roster]
    #pprint(simple_roster)
    roster2 = O.api.roster2()

    # strangely:
    # .roster() (hitting api.facebook.com/graphql) gets *more* contacts than listed on the facebook website
    # .roster2() (hitting graph.facebook.com) gets *less*
    #   the extra contacts are message requests, and your self-profile
    roster_ids = set(int(e['represented_profile']['id']) for e in roster)
    roster2_ids = set(int(e['id']) for e in roster2)

    assert roster2_ids.issubset(roster_ids)

    delta = roster_ids - roster2_ids
    delta = [e for e in roster if int(e['represented_profile']['id']) in delta]
    pprint(delta)
    
    simple_delta = [(int(e['represented_profile']['id']), e['structured_name']['text'], e['represented_profile']['friendship_status'], e['represented_profile']['subscribe_status'], e['represented_profile'].get('is_messenger_user', "unknown_if_messenger_user")) for e in delta]
    pprint(simple_delta)

    #O.loop_forever() # BEWARE: this is blocking but it spawns a thread anyhow
